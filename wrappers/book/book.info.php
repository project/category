name = Wrapper: book
description = Wrapper module to provide a compatibility layer between the book module and the category module.
package = Category
dependencies[] = category_menu
core = 6.x
