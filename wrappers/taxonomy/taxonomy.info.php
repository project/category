name = Wrapper: taxonomy
description = Wrapper module to provide a compatibility layer between the taxonomy module and the category module.
package = Category
dependencies[] = category
core = 6.x
