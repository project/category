<?php

/**
 * Settings form.
 */
function category_admin_settings() {
  $form = array();
  // select options
  $distant = array(
    '0' => t('None'),
    '1' => t('One'),
    '2' => t('Multiple'),
  );

  // Wrapper settings
/*  $form['wrappers'] = array(
    '#type' => 'fieldset',
    '#title' => t('Wrapper install / uninstall settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $book_status = category_get_wrapper_status('book');
  $taxonomy_status = category_get_wrapper_status('taxonomy');
  $form['wrappers']['book_wrapper'] = array(
    '#type' => 'item',
    '#title' => t('Book wrapper status'),
    '#value' => theme('category_wrapper_status', 'book', $book_status),
  );
  $form['wrappers']['taxonomy_wrapper'] = array(
    '#type' => 'item',
    '#title' => t('Taxonomy wrapper status'),
    '#value' => theme('category_wrapper_status', 'taxonomy', $taxonomy_status),
    '#description' => '<p>'. t('You can install and uninstall the taxonomy and book wrapper modules using the links below. It is very important that you have taxonomy or book (original or wrapper, whichever is installed) <strong>enabled</strong> on the <a href="@module-admin-page">module administration page</a> before performing an install or uninstall. Additionally, you should make sure that your web server has write permission on the file system, or the scripts may be denied access to copy and rename the necessary files.', array('@module-admin-page' => url('admin/build/modules'))) .'</p>'
    .'<p>'. t('When performing an install, the scripts will rename your existing taxonomy or book module file to <code>modulename.module.old</code>. It will then copy the appropriate <code>modulename.module.copyme</code> file from the <code>wrappers/</code> directory (in your category package) to the location where the old module file was, and will rename the copied file to <code>modulename.module</code>. The reverse will happen when performing an uninstall. The script will determine the correct location of all necessary files automatically, based on the existing locations of those files.') .'</p>'
    .'<p>'. t('After the operation that you invoke is completed, you will be returned to this page.') .'</p>',
  );
*/
  // Node type settings
  $form['nodetype'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content type settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['nodetype']['category_allow_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allow other content types to be'),
    '#default_value' =>  variable_get('category_allow_nodetypes', array('category_cat' => 0, 'category_cont' => 0)),
    '#options' => array('category_cat' => t('Categories'), 'category_cont' => t('Containers')),
    '#description' => t('Allows category or container behavior to be assigned to other content types. When enabled, each content type (except for category and container) receives a new setting allowing it to be a category or a container. When switched on for a particular content type, all nodes of that type receive the new behavior.')
  );
// Replace the below with "Add / delete default cat/cont type" buttons
/*
  $form['nodetype']['category_base_nodetypes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Provide these content types'),
    '#default_value' =>  variable_get('category_base_nodetypes', array('category_cat' => 'category_cat', 'category_cont' => 'category_cont')),
    '#options' => array('category_cat' => t('Category'), 'category_cont' => t('Container')),
    '#description' => t('Provides a built-in content type for creating categories, and a built-in content type for creating containers. Do not disable either of these unless you have allowed other content types to be categories or containers, and you have set other content types to have category or container behavior; otherwise, you will have not be able to create categories or containers.')
  );
*/

  // Distant parent settings
  $form['distant'] = array(
    '#type' => 'fieldset',
    '#title' => t('Distant parent settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['distant']['category_distant_containers'] = array(
    '#type' => 'radios',
    '#title' => t('Number of parents that a container can have'),
    '#default_value' =>  variable_get('category_distant_containers', 1),
    '#options' => $distant,
    '#description' => t("If set to more than zero, this allows your containers to select other containers, or categories, as distant parents. Changing this setting does not affect your existing containers; unless you change it from 'one' or 'multiple' to 'zero', and you then manually update an individual container, in which case all distant parent relationships for that container will be lost. This setting does not affect distant parent relationships for categories: these are managed separately within each container."),
  );

  // Allow other modules to add additional settings.
  $extra = module_invoke_all('category', 'settings');
  if (isset($extra) && is_array($extra)) {
    foreach ($extra as $key => $value) {
      $form[$key] = $value;
    }
  }

  $form['buttons']['#weight'] = 10;

  return system_settings_form($form);
}