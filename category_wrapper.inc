<?php

/**
 * @file
 * Wrapper management functions for Category.
 */

/**
 * The category wrapper install / uninstall script.
 *
 * @param $type
 *   The wrapper being installed or uninstalled ('taxonomy' or 'book').
 * @param $op
 *   The operation being performed ('install' or 'uninstall').
 */
function _category_wrapper($type = NULL, $op = NULL, $goto = NULL, $rebuild = TRUE) {
  if (!isset($goto)) {
    $goto = 'admin/content/category/settings';
  }
  $generic_error = ' '. t('Unable to perform the specified operation.');

  // Various validation checks
  if (!($type == 'taxonomy' || $type == 'book') || !($op == 'install' || $op == 'uninstall')) {
    drupal_set_message(t('Invalid parameters supplied to wrapper install / uninstall script.') . $generic_error, 'error');
    drupal_goto($goto);
  }
  if (!module_exists($type)) {
    drupal_set_message(t('The %type module is not currently enabled. You must enable it before performing an install or uninstall.', array('%type' => $type)), 'error');
    drupal_goto($goto);
  }
  $status = category_get_wrapper_status($type);
  if (($status && $op == 'install') || (!$status && $op == 'uninstall')) {
    drupal_set_message(t('The %type module is already @status.', array('%type' => $type, '@status' => ($status ? t('installed') : t('uninstalled')))) . $generic_error, 'error');
    drupal_goto($goto);
  }

  $module_path = drupal_get_path('module', $type);
  $module_file = $type .'.module';
  $module_file_path = $module_path .'/'. $module_file;
  if (!file_exists($module_file_path)) {
    drupal_set_message(t('The file %filename could not be found.', array('%filename' => $module_file)) . $generic_error, 'error');
    drupal_goto($goto);
  }
  $info_file = $type .'.info';
  $info_file_path = $module_path .'/'. $info_file;
  if (!file_exists($info_file_path)) {
    drupal_set_message(t('The file %filename could not be found.', array('%filename' => $info_file)) . $generic_error, 'error');
    drupal_goto($goto);
  }
  if ($op == 'install') {
    $wrapper_file = $type .'.module.copyme';
    $wrapper_file_path = drupal_get_path('module', 'category') .'/wrappers/'. $wrapper_file;
    if (!file_exists($wrapper_file_path)) {
      drupal_set_message(t('The file %filename could not be found.', array('%filename' => $wrapper_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    $wrapper_info_file = $type .'.info.copyme';
    $wrapper_info_file_path = drupal_get_path('module', 'category') .'/wrappers/'. $wrapper_info_file;
    if (!file_exists($wrapper_info_file_path)) {
      drupal_set_message(t('The file %filename could not be found.', array('%filename' => $wrapper_info_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    if (!@copy($wrapper_file_path, $module_path .'/'. $wrapper_file)) {
      drupal_set_message(t('The file %filename could not be copied.', array('%filename' => $wrapper_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    if (!@rename($module_file_path, $module_file_path .'.old')) {
      drupal_set_message(t('The file %filename could not be renamed.', array('%filename' => $module_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    $wrapper_new_path = $module_path .'/'. $wrapper_file;
    if (!@rename($wrapper_new_path, $module_file_path)) {
      drupal_set_message(t('The file %filename could not be renamed.', array('%filename' => $wrapper_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    if (!@copy($wrapper_info_file_path, $module_path .'/'. $wrapper_info_file)) {
      drupal_set_message(t('The file %filename could not be copied.', array('%filename' => $wrapper_info_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    if (!@rename($info_file_path, $info_file_path .'.old')) {
      drupal_set_message(t('The file %filename could not be renamed.', array('%filename' => $info_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    $wrapper_info_new_path = $module_path .'/'. $wrapper_info_file;
    if (!@rename($wrapper_info_new_path, $info_file_path)) {
      drupal_set_message(t('The file %filename could not be renamed.', array('%filename' => $wrapper_info_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
  }
  else {
    $old_file = $type .'.module.old';
    $old_file_path = drupal_get_path('module', $type) .'/'. $old_file;
    if (!file_exists($old_file_path)) {
      drupal_set_message(t('The file %filename could not be found.', array('%filename' => $old_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    $old_info_file = $type .'.info.old';
    $old_info_file_path = drupal_get_path('module', $type) .'/'. $old_info_file;
    if (!file_exists($old_info_file_path)) {
      drupal_set_message(t('The file %filename could not be found.', array('%filename' => $old_info_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    if (!@unlink($module_file_path)) {
      drupal_set_message(t('The file %filename could not be deleted.', array('%filename' => $module_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    if (!@rename($old_file_path, $module_file_path)) {
      drupal_set_message(t('The file %filename could not be renamed.', array('%filename' => $old_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    if (!@unlink($info_file_path)) {
      drupal_set_message(t('The file %filename could not be deleted.', array('%filename' => $info_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
    if (!@rename($old_info_file_path, $info_file_path)) {
      drupal_set_message(t('The file %filename could not be renamed.', array('%filename' => $old_info_file)) . $generic_error, 'error');
      drupal_goto($goto);
    }
  }

  if ($type == 'taxonomy') {
    if ($op == 'install') {
      db_query("UPDATE {system} SET weight = 10 WHERE name = 'taxonomy' AND type = 'module'");
    }
    else {
      db_query("UPDATE {system} SET weight = 0 WHERE name = 'taxonomy' AND type = 'module'");
    }
  }

  drupal_set_message(t('The @type wrapper was @op successfully.', array('@type' => $type, '@op' => ($op == 'install' ? t('installed') : t('uninstalled')))));

  if ($rebuild) {
    module_list(TRUE, FALSE);
    menu_rebuild();
    drupal_goto($goto);
  }
}